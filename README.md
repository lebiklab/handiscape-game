#Handiscape Game : le projet

Handiscape game est un jeu vidéo de type escape game solo, destiné à sensibiliser aux handicaps invisibles

Ce projet a été réalisé par une équipe pluri-disciplinaire du collège Suze Angely avec l’accompagnement du BIK’LAB et a obtenu le 1er prix de l’innovation pédagogique

## Le pitch

_2150, les voyages dans le temps font partie du quotidien et la notion de handicap a pratiquement disparu._

_Tu es Nova, une élève du collège Suze Angely. Un problème technique est survenu lors d’une sortie scolaire en 1976 : des camarades de classe sont restés bloqués entre les 2 époques. Tu devras résoudre des énigmes pour les sauver, au risque, sinon de provoquer une grave éruption volcanique, susceptible de détruire la Basse-Terre !_

_Bonne chance !_

## Version en ligne

Le jeu a été créé avec Genially et est accessible en ligne
TODO noter l’url en ligne

## Version autonome

Les accès internet dans les établisesments scolaires ne sont pas toujours aisés, il a donc été créé une version autonome du jeu qui peut être utilisée depuis une clé USB ou depuis un serveur web, sur l’internet ou bien directement sur le réseau local de l’établissemet ou de la classe.

TODO faire une version portable app avec serveur web embarqué

## Contributeurs
TODO mettre la liste des contributeurs au projet
